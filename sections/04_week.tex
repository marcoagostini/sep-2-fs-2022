\section{Quality}
Keep the project management triangle in mind, we always coordinate between: cost, time and scope and the quality of the project. As expected the quality has the most influence of the other three parameters.

\subsection{Example: Therac-25 }
The software project Therac-25 was one of the software projects with the worst quality management. This device has caused over six death accidents because of a software failure.

There where no hardware interlocks to prevent dangerous operation modes.  All software developed by a single person, no details about his/her qualification known. The engineer reused on parts of an existing software developed by former employees. Neither the software, nor the operating system were independently reviewed. Bad UI/UX: error messages merely displayed the word “Malfunction” followed by a number. 

\begin{quote}
	«Primary cause was general poor software design and development practices rather than single-out specific coding errors.» - N. Leveson \& C. Turner
\end{quote}

The quality of software is affected by: processes and people, requirements, UI/UX, analysis and design, coding, reviews, testing, documentation and more!  
  
\subsection{Quality in Software Projects}
\begin{quote}
	«Capability of a software product to conform to requirements.» - ISO/IEC 9001
\end{quote}

\begin{quote}
	«Quality […] is based on the customer's actual experience with the product or service, measured against his or her requirements.» - A.V. Feigenbaum 1983
\end{quote}

There can be a general approach to achieve good software quality.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{quality}
  \caption{A general Approach to Quality in Software Projects}
\end{figure}

\subsubsection{Requirements}
The different types of requirements were already covered in SEP1 and therefore we only do a quick repetition. The non-Functional requirements are in general harder to write down and have a big influence on the technical solution.


\begin{minipage}{0.5\textwidth}
	\textbf{Functional Requirements (What?)}
	\begin{itemize}
		\item Acquired together with the customer
		\item Often gathered by a Requirement Engineer
		\item Generally easy to verify (e.g. functional testing) 
	\end{itemize}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\textbf{Non-Functional (How?)}
	\begin{itemize}
		\item Defined by the customer and the developers
		\item Often gathered by a Software Architect
		\item Harder to verify (e.g. performance tests)
	\end{itemize}
\end{minipage}

\paragraph{ISO/IEC 25010} \hfill \\

\begin{figure}[hbt]
  \center
  \includegraphics[width=\textwidth]{iso25010}
  \caption{ISO 25010 has a clear definition for Non-Functional Requirements}
\end{figure}

\paragraph{Absence of Requirements} \hfill \\
We have a model for the absence of requirements. Over engineering is not that much of a problem. On the other hand, under engineering is a problem in nearly every project: Inappropriate processes and missing roles, Inadequate or inexistent requirements, Poorly educated or equipped staff,  Willingly sacrificing quality to "save money".

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{absencerequirements}
  \caption{The Model for Absence of Requirements}
\end{figure}

\subsubsection{Quality over Time}
Programs, like people, get old. To prevent this decay, we must constantly observe and maintain quality. Keep in mind, that you might also need to adapt the used quality criteria. As most of the time, reasonable automation can dramatically reduce efforts.

\paragraph{Broken Window Theory} \hfill \\
Criminological theory introduced in 1982 and states that visible sings of crime encourage further crimes and the severity of crimes tend to increase. The conclusion is to avoid serious crime we need to prevent minor crimes. This principle can also be applied to software engineering.

\subsection{Quality Measures}
To measure quality we can takes measures in processes, practices and tools. In order to achieve good quality all these pillars should be covered.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{pquality}
  \caption{The three Areas of Quality Measures}
\end{figure}

\subsubsection{Technical Dept}
Reflects the implied cost of additional rework caused by choosing an easy solution. Technical debt is not necessarily a bad thing and sometimes required to move on. As with monetary debt, if technical debt is not repaid, it can accumulate interest. Be aware that the technical debt might not increase linear - try to reduce debt at certain thresholds.

\subsubsection{Metrics}
Can be used to ensure the desired quality standards are met. These metrics are usually calculated automatically with tools as a part of the CI/CD. Examples: Maintainability, Test Coverage, Statement Coverage.

\subsubsection{Dashboards}
Data visualisations helping to understand the actual progress of the project.  Understandable also for non-technical users such as project managers.  Should be created automatically from existing metrics: Process Metrics, Product Metrics.
  
\subsection{Software Metrics}
\begin{quote}
	«A software metric is a standard of measure of a degree to which a software system or process possesses some property.» – Norman Fenton, 2014
\end{quote}

A word of caution regarding software metrics! Metrics are only indicators and might be misleading for certain circumstances. Acceptable metric values do not imply good software quality. Make sure actions are defined for the case a metric outruns the acceptable range. Never use metrics for a (financial) reward system!
   

\paragraph{Example: Maintainability} \hfill \\
\textbf{Requirement:} Maintainability, \textbf{Indicator:} Code covered by automated tests is usually considered as to be maintainable, \textbf{Metric:} Different ways to measure “Test Coverage”, team decides to define a Statement Coverage of >= 90\% as acceptable.

\subsubsection{Categorization}
We can differentiate between product and project metrics. \\
\begin{description}
	\itemsep -0.2em
	\item [Product Metrics] Lines of Code (LOC), Code Coverage (CC)
	\item [Project Metrics] Number of Git-commits, Created bugs in the issue tracker, accumulated technical debt.
\end{description} 


\subsubsection{McCabe's Cyclomatic Complexity (McCC)}
Complexity of the control flow – more branches result in higher values. Defined by McCabe in 1976. The scale is defined by C4 software (1-10 simple programm, 11-20 more complex, 21-50 complex, 50+ unstable).

\[ M= E-N+2P \]
E=Number of Edges, N=Number of Nodes, P=Number of independent graphs.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{mccc}
  \caption{Example for the McCabe's Cyclomatic Complexity Metric}
\end{figure}

\[ M = 8-7+2*1=3\]

\subsubsection{Tooling}
Do not calculate metrics by hand, but make sure you understand their meaning. Tools are frequently changing for every language. Things to consider when using automatic software metrics: integration in IDE, integration in CI/CD, license cost.


\subsubsection{Visualisation of Metrics} 
 Help to find critical metric values faster and detect abnormal outliers and assess system health as whole. Exceptional patterns usually indicate a design or code issue (Smells). 
 
\paragraph{Polymetric View} \hfill \\ 
Represents up to five metrics in a single node (class): Width, Height, Position, Color, Relation.

\begin{figure}[hbt]
  \center
  \includegraphics[width=0.5\textwidth]{polymetric}
  \caption{Polymetric View for a Class}
\end{figure}

\subsection{Conslusion}
\begin{itemize}
	\item Quality is neither good nor bad – it is conforming to requirements (or not) 
	\item Without measurable requirements, you can not assess the quality of your product 
	\item Quality tends to decrease over time, thus maintaining quality is a continuous process 
	\item Metrics can be used to ensure the desired quality standards are met 
	\item Integrate appropriate tools to calculate metrics as part of your CI/CD 
	\item Visualizations are a useful tool for quickly identifying exceptions patterns in your software
\end{itemize}



