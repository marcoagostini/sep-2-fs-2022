\section{Clean Code}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.76\textwidth]{cleancode}
  \caption{Clean Code}
\end{figure}

\begin{quote}
	«Clean code is simple and direct. Clean code reads like well-written prose.» – Grady Booch
\end{quote}

There is a common rule of 1:10 of writing code vs reading code. Even when you write new code you are reading to old code. Writing clean code is a basic skill of every software professional. Also keep the costs in mind when maintaining a software repository in the long run. Maintaining dirty code will be a lot harder in comparison to clean code.

There are two pillars of clean code: knowledge and practice. One you will be able to learn and practice will only be gained with experience and writing a lot of good code. Becoming a good coder is like becoming an artist.

\subsection{Fundamentals}
\textbf{YAGANI:} You aren't gonna need it! \\
Code, that has never been written, can not rot. Also less code means less complexity. And the agiles mindset does state: avoid waste.

\textbf{KISS:} Keep it simple, stupid! \\
Simple code tends to be more readable. If there are options - choose the simple one. Beware of premature optimisations.

\paragraph{Easier to Change (ETC)}
ETC does help when making decisions. Should i use option A or B? - ETC! Always choose the code which is easier to change.

\subsubsection{Naming}
Naming variables, functions, methods consistent and good is very hard.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{naming}
  \caption{Naming in Coding}
\end{figure}

There are also some things to avoid in clear coding.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{namesavoid}
  \caption{Naming Principles to avoid}
\end{figure}

\subsubsection{Functions}
A long parameter list makes it: harder to read the code, harder to change the code, are indicating for a function doing more than one thing (see SRP), are an indicator for a missing domain concept.  \textbf{Treatments:} Split up single function into multiple functions, Introduce Parameter Object.

\paragraph{Function or Query} \hfill \\
Every function should either perform a command (Function) or return data (Query) this concept is known as CQS - command and query separation. Querying functions are also said to be side-effect free. Functions having side-effects violate the POLA - Principle of least astonishment.

\paragraph{Avoid Output Arguments} \hfill \\
Readers expect arguments to be inputs, no outputs (see POLA). The result of a function should be in its result, not in an argument. When a function needs multiple results, it has very likely to match responsibilities. Output arguments are often a violation of CQS.

\begin{lstlisting}
const string input = "42";
int paredInput;
bool couldParse = int.TryParse(input, out parsedInput); // avoid
if (couldParse) { ... }
\end{lstlisting}

\paragraph{Error-Handling} \hfill \\
Use exceptions instead of return codes, as error codes can (and will) be ignored. Exceptions can be evaluated centralized, making the rest of the code more readable. Add a precise description of the error causing the exception in the message. Avoid returning null – use an exception or a Null Object instead.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{namingfunctions}
  \caption{Naming of Functions}
\end{figure}

\subsubsection{Classes}

\paragraph{Cohesiond and Coupling} \hfill \\
Cohesion is the degree to which the elements inside a class belong together. In a highly cohesive system, code readability and reusability are increased. High cohesion often correlates with loose coupling, and vice versa.
 
\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{couplingcohesion}
  \caption{Coupling and Cohesion}
\end{figure}

\paragraph{Law of Demeter (LoD)}
The Law of Demeter says that classes should not know about the details of the objects it manipulates. Therefore, do not invoke functions on objects that are returned by another object. 

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{namingclasses}
  \caption{Naming Classes}
\end{figure}

\subsubsection{Comments}
The most common motivation for writing comments is bad code.  Instead of adding a comment, try to improve your code so it expresses its intention.  Problems with comments: hard to maintain, misleading when outdated, add noise to the code.
 
\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{namingcomments}
  \caption{Use of Comments}
\end{figure}

\subsubsection{Formatting}
 Consistent formatting improves readability.  Teams should agree upon a single style: indents, line breaks, capitalisation, naming etc. Enforce these styling-rules with IDE-Support and reflect the quality over time.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{formatting}
  \caption{Formatting Do's!}
\end{figure}


\subsection{Design Principles}
\subsubsection{DRY - Don't Repeat Yourself}
Duplication: primary enemy of maintainability in code, documentation, just everywhere!

\subsubsection{SOLID}
An acronym for five design principles intended to make software designs: Understandable, Flexible,  Maintainable. These principles are the building blocks of many design patterns. There are different mechanisms to reach this goal: Template Method, Strategy Design Pattern.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{solid}
  \caption{S.O.L.I.D}
\end{figure}

\paragraph{SRP - Single-Responsibility Principle} \hfill \\
A class, module or function should have only one single reason to change. Compose your software of many small classes, not a few large ones. There are some benefits: Readability, Maintainability, Reusability, Testability.

\paragraph{OCP - Open-Closed Principle} \hfill \\
Software entities should be open extension but closed for modification. It should be possible to add functionality without changing existing code.

\begin{lstlisting}{caption=OCP}
public void DoSomethingFancy() {
	Console.WriteLine("Beginning of method"); 
	// omitted to reduce complexity 
	Console.WriteLine("End of method");
}

public void DoSomethingFancy(Action<string> log) {
	log("Beginning of method"); 
	// omitted to reduce complexity 
	log("End of method");
}
\end{lstlisting}

\paragraph{LSP - Liskov Subtitution Prinviple} \hfill \\
The subtypes must behave like their base type. A subtype may extend base type functionality, but must not reduce it. Clients working with base types do not know about any specifics of sub-types.

\textbf{Examples:} Adding more functions to subtype, Throwing new exception-types in subtype, Checking concrete type in client-code.
 
\paragraph{ISP - Interface Segregation Principle} \hfill \\
Try to create small interface which group one functionality. A client shall only depend on the details which it does use. This goal can be reached by extracting interfaces and super classes. The leaner the service interface is, the smaller is the coupling between components. Also increases readability, as it is more clear which function to use.

\paragraph{DIP - Dependency Inversion Principle} \hfill \\
High-level classes must not depend on low-level classes but both from i nterfaces. This way, the low-level class can be replaced, which is very beneficial fro testing. A common way to resolve an abstract dependency is dependency injection: constructor injection, Inversion of Control Container (IoCC). The principle has strong synergies with OCP.

\paragraph{ADP - Acyclic Dependencies Principle} \hfill \\
A circular dependency is a relation between two or more class, which directly or indirectly depend on each other. The problems of circular dependencies: lead to tight coupling, Decrease testability, decrease re-usability. 

The ADP states that the dependency graph of packages or components should have no cycles. 

\paragraph{SLA - Single Level of Abstraction}
Keeping one level of abstraction fosters readability: one level within a package, one level within a class, one level within a function. Remember the newspaper metaphor: headings, first paragraph, full text.

\subsection{Test-Driven Development (TDD)}
As its name implies: development is driven by tests. Unit tests are created before the productive code is written. TDD is a technical practice from Extreme Programming.  XP and TDD were both developed by Kent Beck around the year of 2000.

\subsubsection{Unit Testing}
Unit Tests are your safety net!

A unit test is a piece of code ensuring that a small section of an application behaves as intended. These small sections are also called Units. Every Unit is tested on its own, i.e., independently from all other code.  Dependencies are resolved using Test Doubles like Mocks, Fakes, etc.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.4\textwidth]{unittesting}
  \caption{Unit Testing}
\end{figure}

\subsubsection{TDD Cycle}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.5\textwidth]{tddcycle}
  \caption{TDD Cycle}
\end{figure}

There are multiple reasons to use TDD:
\begin{itemize}
	\item  You will be no longer afraid to change code 
	\item Your code will be simpler
	\item Your code will be less coupled
	\item Your code will be testable per design
	\item You will spend way less time debugging
	\item You get motivated by fast feedback cycles
	\item Your code get a built-in how to use-manual
\end{itemize}

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{tddlimits}
  \caption{TDD Limits}
\end{figure}

